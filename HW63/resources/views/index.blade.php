@extends('layouts.app')
@section('content')
<div class="container" style="border-left: 1px solid black; border-right: 1px solid black; border-bottom: 1px solid black">
    @foreach($photos as $photo)
        <div style="margin-bottom: 150px">
            @if($photo->user->main_photo == null)
            <a href="{{action([\App\Http\Controllers\UserController::class, 'showOther'], ['user' => $photo->user])}}"><img style="height: 50px; width: 50px" src="{{asset('/storage/images/' . 'img.png')}}"></a>
            @else
            <a href="{{action([\App\Http\Controllers\UserController::class, 'showOther'], ['user' => $photo->user])}}"><img style="height: 50px; width: 50px" src="{{asset('/storage/images/' . $photo->user->main_photo)}}"></a>
            @endif
            <p style="float: right; margin-right: 1100px">{{$photo->user->name}}</p>
            <a style="text-decoration: none; color: black" href="{{action([\App\Http\Controllers\PhotoController::class, 'show'], ['photo' => $photo])}}">
                <img style="height: 400px; width: 500px; border: 1px solid black" src="{{asset('/storage/images/' . $photo->name)}}">
                <p>Комментариев: {{$photo->comments->count()}}</p>
                <p style=" margin-right: 950px; margin-bottom: -100px">Лайков: {{$photo->likes}}</p>
            </a>
        </div>
    @endforeach
</div>
@endsection

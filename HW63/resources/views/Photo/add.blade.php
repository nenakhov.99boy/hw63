@extends('layouts.app')
@section('content')
    <div class="container">
        <form enctype="multipart/form-data" method="post" action="{{action([\App\Http\Controllers\PhotoController::class, 'store'])}}">
            @csrf
            <div>
                <input name="name" type="file" id="name">
                <input type="hidden" name="user_id" id="User_id" value="{{$user->id}}">
            </div>
            <button type="submit">Добавить</button>
        </form>
    </div>
@endsection

@extends('layouts.app')
@section('content')
    <div class="container">
        <h1>Выберите новую аватарку</h1>
        <div class="row">
        @foreach($user->photos as $photo)
            <div class="col-2">
            <form method="post" action="{{action([\App\Http\Controllers\PhotoController::class, 'storeMain'])}}">
                @csrf
                <img style="height: 150px; width: 150px; border: 1px solid black" src="{{asset('/storage/images/' . $photo->name)}}">
                <input type="hidden" name="name" id="name" value="{{$photo->name}}">
                <input type="hidden" name="user_id" id="User_id" value="{{$user->id}}">
                <div style="text-align: center">
                    <button type="submit">Выбрать</button>
                </div>
            </form>
            </div>
        @endforeach
        </div>
    </div>
@endsection

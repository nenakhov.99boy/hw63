@extends('layouts.app')
@section('content')
    <div class="container">
        <div style="border: 1px solid black; height: 250px">
            <div style="float: left; padding-top: 20px; padding-left: 20px">
                @if($user->main_photo == null)
                    <a href="{{action([\App\Http\Controllers\PhotoController::class, 'changeMain'])}}">
                    <img style="height: 200px; border: 1px solid black; border-radius: 100px; width: 200px" src="{{asset('/storage/images/' . 'img.png')}}" alt="photo">
                    </a>
                @else
                    <a href="{{action([\App\Http\Controllers\PhotoController::class, 'changeMain'])}}">
                    <img style="height: 200px; border: 1px solid black; border-radius: 100px; width: 200px" src="{{asset('/storage/images/' . $user->main_photo)}}">
                    </a>
                @endif
            </div>
            <div style="position: absolute; left: 600px">
                <h1 style="padding-top: 60px">{{$user->name}}</h1>
            </div>
            <div style="margin-top: 170px; padding-left: 450px">
                <form>
                <a href="{{action([\App\Http\Controllers\PhotoController::class, 'create'])}}">Добавить фото</a>
                </form>
            </div>
        </div>
        <div>
            <h1>Галлерея</h1>
            @foreach($user->photos as $photo)
                <a href="{{action([\App\Http\Controllers\PhotoController::class, 'show'], ['photo' => $photo])}}">
                    <img style="height: 150px; width: 150px; border: 1px solid black" src="{{asset('/storage/images/' . $photo->name)}}">
                </a>
            @endforeach
        </div>
        <div class="row">
            <h1>Подписчики</h1>
            @foreach($user->followers as $follower)
                @if($follower->photo == null)
                    <img style="height: 30px; width: 40px; border: 1px solid black; border-radius: 100px" src="{{asset('/storage/images/' . 'img.png')}}">
                @else
                    <img style="height: 30px; width: 30px; border: 1px solid black; border-radius: 100px" src="{{asset('/storage/images/' . $follower->photo)}}">
                @endif
            @endforeach
        </div>
    </div>
@endsection

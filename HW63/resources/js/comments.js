$(document).ready(()=>{
    $('#like-button').on('click', function () {
        const photo = $('.photo-for-like');
        if ($(this).hasClass('no-click'))
        {
            $.ajax({
                url: `/photo/${photo}/likeIncrease/`,
                method: 'GET',
            })
            .done(function (msq) {
                $(this).removeClass('no-click');
                $(this).addClass('click-one');
                $('#like').attr('style', 'color:green');
            })
        }
        else {
            $(this).removeClass('click-one');
            $(this).addClass('no-click');
            $('#like').attr('style', 'color:black');
        }
    })
})

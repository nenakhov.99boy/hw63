<?php

namespace App\Http\Controllers;

use App\Models\Follower;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show()
    {
        $user = Auth::user();
        return view('User.show', compact('user'));
    }

    public function index()
    {
        $users = User::all();
        return view('User.index', compact('users'));
    }

    public function showOther(User $user)
    {
        $authUser = Auth::user();
        return view('User.showOther', compact('user', 'authUser'));
    }

    public function storeFollower(User $user)
    {
        $acceptingUser = $user;
        $subscriber = Auth::user();
        $follower =  new Follower();
        $follower->name = $subscriber->name;
        $follower->email = $subscriber->email;
        $follower->user_id = $subscriber->id;
        $follower->photo = $subscriber->main_photo;
        $follower->save();
        $array = ['user_id' => $acceptingUser->id];
        $follower->users()->attach($follower, $array);
        return redirect()->back()->with('status', 'Подписка оформлена!');
    }
}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\PhotoStoreRequest;
use App\Models\Photo;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class PhotoController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create()
    {
        $user = Auth::user();
        return view('Photo.add', compact('user'));
    }

    public function store(PhotoStoreRequest $request)
    {
        $validated = $request->validated();
        $image = data_get($validated, 'name');
        if ($image) {
            $fileName = $image->getClientOriginalName();
            $path = $image->storeAs('images', $fileName, 'public');
            $validated['name'] = basename($path);
        }
        $photo = new Photo($validated);
        $photo->likes = 0;
        $photo->save();
        return redirect('/');
    }

    public function show(Photo $photo)
    {
        $user = Auth::user();
        return view('Photo.show', compact('photo', 'user'));
    }


    public function changeMain()
    {
        $user = Auth::user();
        return view('Photo.changeMain', compact('user'));
    }

    public function storeMain (Request $request)
    {
        $user = User::where('id', $request['user_id'])->first();
        $user->main_photo = $request['name'];
        $user->save();
        return redirect('/');
    }

    public function likeIncrease(Photo $photo)
    {
        $photo->likes++;
        $photo->save();
    }

    public function likeDecrease(Photo $photo)
    {
        $photo->likes--;
        $photo->save();
    }
}

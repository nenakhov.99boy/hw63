<?php

namespace App\Http\Controllers;

use App\Models\Photo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class RootController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
     public function index()
     {
         $photos = Photo::all()->sortByDesc('created_at');
         return view('index', compact('photos'));
     }
}

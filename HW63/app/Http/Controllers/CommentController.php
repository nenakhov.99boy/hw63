<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentStoreRequest;
use App\Models\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(CommentStoreRequest $request)
    {
        $validated = $request->validated();
        $comment = new Comment($validated);
        $comment->save();
        return redirect()->back();
    }

}

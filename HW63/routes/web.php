<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [\App\Http\Controllers\UserController::class, 'show']);
Route::get('/users/{user}/', [\App\Http\Controllers\UserController::class, 'showOther']);
Route::get('/users/{user}/follow', [\App\Http\Controllers\UserController::class, 'storeFollower']);

Route::get('/photo/add/', [\App\Http\Controllers\PhotoController::class, 'create']);
Route::post('/photo/store', [\App\Http\Controllers\PhotoController::class, 'store']);
Route::get('/photo/main/', [\App\Http\Controllers\PhotoController::class, 'changeMain']);
Route::post('/photo/storeMain/', [\App\Http\Controllers\PhotoController::class, 'storeMain']);
Route::get('/photo/{photo}/', [\App\Http\Controllers\PhotoController::class, 'show']);
Route::get('/photo/{photo}/likeIncrease/', [\App\Http\Controllers\PhotoController::class, 'likeIncrease']);
Route::get('/photo/{photo}/likeDecrease/', [\App\Http\Controllers\PhotoController::class, 'likeDecrease']);

Route::post('/comment/store/', [\App\Http\Controllers\CommentController::class, 'store']);

Route::get('/lenta/', [\App\Http\Controllers\RootController::class, 'index']);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
